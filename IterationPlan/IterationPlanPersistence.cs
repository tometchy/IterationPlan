﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using IterationPlan.Domain;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;

// ReSharper disable StringLiteralTypo -> It's correct time format
// ReSharper disable ClassNeverInstantiated.Global -> It's instantiated by di container

namespace IterationPlan
{
    public class IterationPlanPersistence : IIterationPlanPersistence
    {
        private readonly ILogger _logger;
        private const string PERSISTENCE_DIRECTORY = "Persitence";
        private const string DATE_TIME_FORMAT = "yyyy_MM_dd__HH_mm_ss_ffff";
        private const string FILE_PREFIX = "iteration_";

        public IterationPlanPersistence(ILogger<IterationPlanPersistence> logger)
        {
            _logger = logger;
        }

        public async Task PersistAsync(Iteration iteration)
        {
            try
            {
                if (iteration.Equals(await GetTheNewestIterationPlanAsync(iteration.Id)))
                {
                    _logger.LogInformation("Persisted iteration is up to date, skipping");
                    return;
                }
                
                var filePath = Path.Combine(PERSISTENCE_DIRECTORY, CreateMandaysFilePath());
                var json = JsonConvert.SerializeObject(iteration, new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver(),
                    Converters = new List<JsonConverter>{new StringEnumConverter {CamelCaseText = true}},
                    NullValueHandling = NullValueHandling.Ignore
                });
                
                _logger.LogInformation($"Persiting iteration: '{json}' to file: '{filePath}'");
                Directory.CreateDirectory(PERSISTENCE_DIRECTORY);
                await File.WriteAllTextAsync(filePath, json);
            }
            catch (Exception e)
            {
                _logger.LogError(e, $"Could not persist iteration: {iteration}"); // ToDo: Structured logging
            }

            string CreateMandaysFilePath() => FILE_PREFIX + iteration.Id + "_" + DateTime.Now.ToString(DATE_TIME_FORMAT) + ".json";
        }

        public async Task<Iteration> GetTheNewestIterationPlanAsync(string id)
        {
            try
            {
                var allPlans = Directory.GetFiles(PERSISTENCE_DIRECTORY, $"{FILE_PREFIX}{id}*.json", SearchOption.TopDirectoryOnly);
                var theNewestPlan = allPlans.OrderByDescending(plan => plan).First();

                _logger.LogDebug($"Getting the newest iteration plan from file: '{theNewestPlan}'");
                var iterationJson = await File.ReadAllTextAsync(theNewestPlan);
                var iteration = JsonConvert.DeserializeObject<Iteration>(iterationJson);
                if(string.IsNullOrWhiteSpace(id) == false)
                    iteration.Id = id;
                return iteration;
            }
            catch (Exception e)
            {
                _logger.LogError(e, $"Could not get the newest iteration plan for id: {id}");
                throw new ArgumentException($"Could not get the newest iteration plan for id: {id}");
            }
        }
    }
}