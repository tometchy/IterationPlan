﻿using System.Threading.Tasks;
using IterationPlan.Domain;

namespace IterationPlan
{
    public interface IIterationPlanPersistence
    {
        Task PersistAsync(Iteration iteration);
        Task<Iteration> GetTheNewestIterationPlanAsync(string id);
    }
}