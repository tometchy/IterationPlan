var app = null;
var connection = null;
window.onload = function () {
    app = new Vue({
        el: '#app',
        data: {
            iteration: {
                users: [],
                mandays: {},
                tasks: [],
                planningDate: "",
                nextPlanningDate: "",
                id: ""
            },

            tasksAreUnderEdit: false,
            tasksUnderEdit: "",
            datesAreUnderEdit: false,
            planningDateUnderEdit: "",
            nextPlanningDateUnderEdit: ""

        },
        computed: {
            iterationDays: function () {
                var startDate = moment(this.iteration.planningDate);
                var endDate = moment(this.iteration.nextPlanningDate).add(-1, 'days');

                var dates = [];
                for (var date = moment(startDate); date.diff(endDate) <= 0; date.add(1, 'days'))
                    dates.push(date.format());
                return dates;
            }
        },
        watch: {
            iteration: {
                handler: function (newIteration) {
                    var iteration = {
                        mandays: convertMandaysDictionaryToMandaysArray(newIteration.mandays),
                        users: newIteration.users,
                        planningDate: newIteration.planningDate,
                        nextPlanningDate: newIteration.nextPlanningDate,
                        tasks: newIteration.tasks,
                        id: newIteration.id
                    };
                    connection.invoke("ChangeIteration", iteration);
                },
                deep: true
            }
        },
        methods: {
            prettyPrintDate: function (date) {
                return moment(date).format("dddd (DD MMM)");
            },
            prettyPrintTask: function (task, maxTaskNameLength) {
                if(task.id.toLowerCase() === "dayoff")
                    return task.name;
                
                var taskId = "#" + task.id + " (" + task.points + "p)" + " - ";
                if (task.name.length > maxTaskNameLength)
                    return taskId + task.name.substring(0, maxTaskNameLength) + "...";
                return taskId + task.name;
            },
            mandayContainsDayOff: function (date, userId) {
                var tasksFromManday = this.getTasksFromManday(date, userId);

                if (tasksFromManday == null) 
                    return false;

                for (var i = 0, length = tasksFromManday.length; i < length; i++) {
                    var task = tasksFromManday[i];
                    if(task !== null) {
                        if(task.id.toLowerCase() === "dayoff")
                            return true;
                    }
                }
                
                return false;
            },
            getTasksFromManday: function (date, userId) {
                var mandayId = createMandayId(date, userId);
                var mandayTaskIds = this.iteration.mandays[mandayId];
                if (mandayTaskIds == null)
                    return null;

                var tasksFromManday = [];
                var tasks = this.iteration.tasks;

                mandayTaskIds.forEach(function (taskId) {
                    var foundTask = null;
                    
                    if(taskId.toLowerCase() !== "dayoff"){
                        foundTask = tasks.find(function (task) {
                            return task.id === taskId;
                        });
                    }
                    else {
                         foundTask = createTask('DAYOFF', "DAY OFF");
                    }

                    if (foundTask != null)
                        tasksFromManday.push(foundTask);
                });

                return tasksFromManday;
            },
            createTaskInIterationTableId: function (mandayId, taskId) {
                return createTaskInIterationTableId(mandayId, taskId);
            },
            createMandayId: function (date, userId) {
                return createMandayId(date, userId);
            },
            createTaskSourceBoxId: function (taskId) {
                return createTaskSourceBoxId(taskId);
            },
            createTaskInSourceBoxId: function (taskId) {
                return createTaskInSourceBoxId(taskId);
            },
            tryAddTaskToManday: function (taskToAddId, mandayId) {
                if (this.iteration.mandays[mandayId] == null)
                    Vue.set(this.iteration.mandays, mandayId, [taskToAddId]);
                else {
                    if (this.iteration.mandays[mandayId].includes(taskToAddId) === false)
                        this.iteration.mandays[mandayId].push(taskToAddId);
                    else
                        return false;
                }
                return true;
            },
            tryRemoveTaskFromManday: function (mandayId, taskId) {
                if (this.iteration.mandays[mandayId] == null)
                    return;
                let taskIndex = this.iteration.mandays[mandayId].indexOf(taskId);
                if (taskIndex > -1) {
                    this.iteration.mandays[mandayId].splice(taskIndex, 1);
                }
            },
            setIteration: function (iteration) {
                if (areEqual(iteration, this.iteration) === false)
                    this.iteration = iteration;
            },
            beginTasksEdition: function () {
                this.tasksUnderEdit = jsonToCsv(this.iteration.tasks, false);
                this.tasksAreUnderEdit = true;
            },
            finishTasksEdition: function () {
                try {
                    var tasks = csvToJson(this.tasksUnderEdit, "id;name;points");
                    tasks.forEach(function (task) {
                        if (isNullOrWhitespaceString(task.id))
                            throw new Error("Wrong task id");
                        if (isNullOrWhitespaceString(task.name))
                            throw new Error("Wrong task name");
                        if (isNullOrWhitespaceString(task.points))
                            throw new Error("Wrong task points");
                        if (typeof task.points === 'string' || task.points instanceof String)
                            task.points = parseFloat(task.points.replace(',', '.'));
                    });
                    this.iteration.tasks = tasks;
                    this.tasksAreUnderEdit = false;
                }
                catch (err) {
                    alert("Incorrect csv: " + err.message)
                }
            },
            finishIterationEdition: function(){
                this.finishDatesEdition();
                this.finishTasksEdition();
            },
            cancelTasksEdition: function () {
                this.tasksAreUnderEdit = false;
            },
            cancelIterationEdition: function() {
                this.cancelDatesEdition();
                this.cancelTasksEdition();
            },
            beginIterationEdition: function(){
                this.beginDatesEdition();
                this.beginTasksEdition();
            },
            beginDatesEdition: function () {
                this.planningDateUnderEdit = this.iteration.planningDate;
                this.nextPlanningDateUnderEdit = this.iteration.nextPlanningDate;
                this.datesAreUnderEdit = true;
            },
            finishDatesEdition: function () {
                try {
                    if (new Date(this.planningDateUnderEdit) >= new Date(this.nextPlanningDateUnderEdit))
                        throw new Error("next planning date must be bigger than planning date");

                    this.iteration.planningDate = this.planningDateUnderEdit;
                    this.iteration.nextPlanningDate = this.nextPlanningDateUnderEdit;
                    this.datesAreUnderEdit = false;
                }
                catch (err) {
                    alert("Incorrect date: " + err.message)
                }
            },
            cancelDatesEdition: function () {
                this.datesAreUnderEdit = false;
            }
        }
    });

    connection = new signalR.HubConnectionBuilder()
        .withUrl('/iterationPlanHub')
        .build();

    connection.on('iterationChanged', setIteration);

    connection.start().then(() => {
            var iterationId = window.location.hash.replace(new RegExp("^#"), "");
            $.get('/iteration/' + iterationId, function (iterationPlan) {
                setIteration(iterationPlan)
            }).fail(function () {
                alert("Could not find iteration: " + iterationId);
            });
        }
    );
};

window.onhashchange = function() {
    var iterationId = window.location.hash.replace(new RegExp("^#"), "");
    console.log("Change iteration invoked: " + iterationId);
    
    $.get('/iteration/' + iterationId, function (iterationPlan) {
        setIteration(iterationPlan)
    }).fail(function () {
        alert("Could not find iteration: " + iterationId);
        var previousHash = '#' + app.iteration.id;
        console.log("Returning to previous hash: " + previousHash);
        window.location.hash = previousHash;
    });
};

function createTask(id, name, points, status){
    if(name == null)
        name = "";
    if(points == null)
        points = 0;
    if(status == null)
        status = 'new';

    return {
        id: id,
        name: name,
        points: points,
        status: status
    };
}

function areEqual(item1, item2) {

    return JSON.stringify(item1) === JSON.stringify(item2)
}

function setIteration(iteration) {
    if (iteration == null)
        return;

    if (iteration.mandays != null) {
        iteration.mandays = convertMandaysArrayToMandaysDictionary(iteration.mandays);
    }

    app.setIteration(iteration);
}

function convertMandaysDictionaryToMandaysArray(mandaysDictionary) {
    var mandays = [];
    for (var mandayId in mandaysDictionary) {
        var taskIds = mandaysDictionary[mandayId];
        if (taskIds.length > 0) {
            mandays.push({
                Id: mandayId,
                taskIds: taskIds
            })
        }
    }
    return mandays;
}

function convertMandaysArrayToMandaysDictionary(mandaysArray) {
    var mandaysDictionary = {};
    if (mandaysArray.length != null && mandaysArray.length > 0) {
        mandaysArray.forEach(function (manday) {
            var mandayId = manday.id;
            mandaysDictionary[mandayId] = manday.taskIds;
        });
    }
    return mandaysDictionary;
}

function jsonToCsv(items, addHeaders) {
    if (items == null || items.length === 0)
        return "";
    const replacer = (key, value) => value === null ? '' : value;
    const header = Object.keys(items[0]);
    let csv = items.map(row => header.map(fieldName => JSON.stringify(row[fieldName], replacer)).join(';'));
    if (addHeaders)
        csv.unshift(header.join(','));
    return csv.join('\r\n');
}

function csvToJson(input, csvHeaders) {
    input = csvHeaders + "\n" + input;
    input = removeEmptyLines(input);
    return $.csv.toObjects(input, {separator: ';', delimiter: '"', headers: true});
}

function removeEmptyLines(input) {
    while (input.includes("\r\n\r\n") || input.includes("\n\n"))
        input = input.replace("\r\n\r\n", "\r\n").replace("\n\n", "\n");
    return input;
}

function isNullOrWhitespaceString(input) {
    if (input == null)
        return true;

    if (typeof input === 'string' || input instanceof String)
        return input.replace(/\s/g, '').length < 1;

    return false;
}

function allowDrop(ev) {
    ev.preventDefault();
}

function drag(ev) {
    ev.dataTransfer.setData("text", ev.target.id);
}

function drop(ev) {
    ev.preventDefault();
    var itemToMoveId = ev.dataTransfer.getData("text");
    var target = ev.target;

    while (isManday(target) === false && isTaskSource(target) === false && target.parentNode != null)
        target = target.parentNode;

    if (target.parentNode == null)
        return;

    let taskToMoveId = getTaskId(itemToMoveId);
    if (isManday(target)) {
        let moveSuccessful = app.tryAddTaskToManday(taskToMoveId, target.id);

        if (moveSuccessful && comesFromIterationTable(itemToMoveId))
            app.tryRemoveTaskFromManday(getMandayIdFromItemToMove(itemToMoveId), taskToMoveId)
    }

    if (isTaskSource(target)) // todo: Rename task source to tasks table
    {
        if (comesFromIterationTable(itemToMoveId))
            app.tryRemoveTaskFromManday(getMandayIdFromItemToMove(itemToMoveId), taskToMoveId)

    }
}

function getMandayIdPrefix() {
    return 'manday'
}

function getTaskSourceBoxIdPrefix() {
    return 'taskSourceBox'
}

function getTaskInSourceBoxIdPrefix() {
    return 'taskInSourceBox'
}

function getTaskInIterationTableIdPrefix() {
    return "taskInIterationTable_";
}

function createTaskInIterationTableId(mandayId, taskId) {
    return getTaskInIterationTableIdPrefix() + mandayId + "_" + taskId;
}

function comesFromIterationTable(itemId) {
    return itemId.startsWith(getTaskInIterationTableIdPrefix());
}

function isManday(target) {
    if (checkHasId(target) === false)
        return false;
    return target.id.startsWith(getMandayIdPrefix());
}

function isTaskSource(target) {
    if (checkHasId(target) === false)
        return false;
    return target.id.startsWith(getTaskSourceBoxIdPrefix());
}

function createMandayId(date, userId) {
    return getMandayIdPrefix() + moment(date).format("YYYYMMDD") + userId;
}

function createTaskSourceBoxId(taskId) {
    return getTaskSourceBoxIdPrefix() + "_" + taskId;
}

function createTaskInSourceBoxId(taskId) {
    return getTaskInSourceBoxIdPrefix() + "_" + taskId;
}

function checkHasId(target) {
    if (target == null)
        return false;

    if (target.id == null)
        return false;

    return target.id !== '';
}

function getTaskId(itemId) {
    return itemId.substring(itemId.lastIndexOf("_") + 1).trim();
}

function getMandayIdFromItemToMove(item) {
    return item
        .slice(0, item.lastIndexOf("_"))
        .replace(getTaskInIterationTableIdPrefix(), "")
        .trim();

}

var isNavOpen=false;

function toggleNav() {
    if(isNavOpen === true)
        closeNav();
    else
        openNav()
}

function openNav() {
    var margin = 20;
    var tasksListWidth = document.getElementById('tasksList').scrollWidth + margin;
    document.getElementById("mySidenav").style.width = tasksListWidth + "px";
    document.getElementById("app").style.marginRight = tasksListWidth + "px";
    isNavOpen=true;
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "150px";
    document.getElementById("app").style.marginRight= "150px";
    isNavOpen=false;
}

var keysPressed = [], shiftCode = 16;

function isShiftPressed() {
    return keysPressed.indexOf(shiftCode) >= 0;
}

$(document).on("keyup keydown", function(e) {
    switch(e.type) {
        case "keydown" :
            keysPressed.push(e.keyCode);
            break;
        case "keyup" :
            var idx = keysPressed.indexOf(e.keyCode);
            if (idx >= 0)
                keysPressed.splice(idx, 1);
            break;
    }
});

jQuery(function ($) {
    $.fn.hScroll = function (amount) {
        amount = amount || 120;
        $(this).bind("DOMMouseScroll mousewheel", function (event) {
            if(isShiftPressed()){
                var oEvent = event.originalEvent,
                    direction = oEvent.detail ? oEvent.detail * -amount : oEvent.wheelDelta,
                    position = $(this).scrollLeft();
                position += direction > 0 ? -amount : amount;
                $(this).scrollLeft(position);
                event.preventDefault();
            }
        })
    };
});

$(document).ready(function() {
    $('#iterationTable').hScroll(60); // You can pass (optionally) scrolling amount
});