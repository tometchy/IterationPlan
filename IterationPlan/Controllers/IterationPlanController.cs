using System.Threading.Tasks;
using IterationPlan.Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace IterationPlan.Controllers
{
    public class IterationPlanController : Controller
    {
        private readonly ILogger _logger;
        private readonly IIterationPlanPersistence _persistence;

        public IterationPlanController(ILogger<IterationPlanController> logger, IIterationPlanPersistence persistence)
        {
            _logger = logger;
            _persistence = persistence;
        }

        [Route("/iteration/{*id}")]
        public async Task<Iteration> Get(string id)
        {
            if (id is null)
                id = string.Empty;
            
            _logger.LogInformation($"Taking iteration plan for iteration: '{id}'");
            var theNewestIterationPlan = await _persistence.GetTheNewestIterationPlanAsync(id);
            
            _logger.LogDebug($"Returning the newest iteration plan(for id: '{id}'): '{theNewestIterationPlan}'");
            return theNewestIterationPlan;
        }
    }
}