﻿// ReSharper disable ClassNeverInstantiated.Global -> It's serialized into json
// ReSharper disable UnusedMember.Global -> It's serialized into json

using Newtonsoft.Json;

namespace IterationPlan.Domain
{
    public class Iteration
    {
        public string Id { get; set; }
        public Manday[] Mandays { get; set; }
        public User[] Users { get; set; }
        public IterationTask[] Tasks { get; set; }
        public string PlanningDate { get; set; }
        public string NextPlanningDate { get; set; }

        public bool Equals(Iteration otherIteration)
        {
            var thisIterationJson = JsonConvert.SerializeObject(this);
            var otherIterationJson = JsonConvert.SerializeObject(otherIteration);
            return thisIterationJson.Equals(otherIterationJson);
        }
    }
}