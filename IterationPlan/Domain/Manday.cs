﻿// ReSharper disable ClassNeverInstantiated.Global -> It's serialized into json
// ReSharper disable UnusedMember.Global -> It's serialized into json

namespace IterationPlan.Domain
{
    public class Manday
    {
        public string Id { get; set; }
        public string[] TaskIds { get; set; }
    }
}