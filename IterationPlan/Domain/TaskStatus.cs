﻿namespace IterationPlan.Domain
{
    public enum TaskStatus
    {
        New,
        Safe,
        Endangered,
        Finished
    }
}