﻿// ReSharper disable ClassNeverInstantiated.Global -> It's serialized into json
// ReSharper disable UnusedMember.Global -> It's serialized into json

namespace IterationPlan.Domain
{
    public class User
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}