﻿using System.Threading.Tasks;
using IterationPlan.Domain;
using Microsoft.AspNetCore.SignalR;
// ReSharper disable ClassNeverInstantiated.Global -> For signalR

namespace IterationPlan.Hubs
{
    public class IterationPlanHub : Hub
    {
        private readonly IIterationPlanPersistence _persistence;

        public IterationPlanHub(IIterationPlanPersistence persistence)
        {
            _persistence = persistence;
        }

        public async Task ChangeIteration(Iteration iteration)
        {
            await Clients.Others.SendAsync("iterationChanged", iteration);
            await _persistence.PersistAsync(iteration);
        }
    }
}
