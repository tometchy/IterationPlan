﻿using IterationPlan.Hubs;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace IterationPlan
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSignalR();
            services.AddMvc();
            services.AddSignalR().AddJsonProtocol(options =>
            {
                options.PayloadSerializerSettings.Converters.Add(new StringEnumConverter {CamelCaseText = true});
                options.PayloadSerializerSettings.NullValueHandling = NullValueHandling.Ignore;
            });
            
            services.AddScoped<IIterationPlanPersistence, IterationPlanPersistence>();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();

            app.UseFileServer();
            
            app.UseSignalR(router => router.MapHub<IterationPlanHub>("/iterationPlanHub"));

            app.UseMvcWithDefaultRoute();
        }
    }
}
